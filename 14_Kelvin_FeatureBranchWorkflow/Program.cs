﻿using System;

namespace _Kelvin_FeatureBranchWorkflow
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int b = 0;
            int total = 0;

            Console.WriteLine("Please enter a number: ");
            int a = Convert.ToInt32(Console.ReadLine());

            while (b < a)
            {
                b++;
                Console.WriteLine(b);
                total += b;
            }

            Console.WriteLine("The sum is:" + total);
        }
    }
}